package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserOrder {
	@Id
	private int sno;
	private int Item_Id;
	private String Item_Name;
	private String Item_Type;
	private float item_Price;
	private String name;
	
	public UserOrder() {
		super();
	}
	
	

	public UserOrder(int sno, int itemId, String itemName, String itemType, float itemPrice, String name) {
		super();
		this.sno = sno;
		Item_Id = itemId;
		Item_Name = itemName;
		Item_Type = itemType;
		this.item_Price = itemPrice;
		this.name = name;
	}



	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public int getItemId() {
		return Item_Id;
	}
	public void setItemId(int itemId) {
		Item_Id = itemId;
	}
	public String getItemName() {
		return Item_Name;
	}
	public void setItemName(String itemName) {
		Item_Name = itemName;
	}
	public String getItemType() {
		return Item_Type;
	}
	public void setItemType(String itemType) {
		Item_Type = itemType;
	}
	public float getItemPrice() {
		return item_Price;
	}
	public void setItemPrice(float itemPrice) {
		this.item_Price = itemPrice;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Order [sno=" + sno + ", ItemId=" + Item_Id + ", ItemName=" + Item_Name + ", ItemType=" + Item_Type
				+ ", itemPrice=" + item_Price + ", name=" + name + "]";
	}

	
	

}
