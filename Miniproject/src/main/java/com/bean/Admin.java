package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {
	@Id
private String admin_email;
private String admin_password;

public Admin() {
	super();
}

public Admin(String email, String password) {
	super();
	this.admin_email = email;
	this.admin_password = password;
}

public String getEmail() {
	return admin_email;
}
public void setEmail(String email) {
	this.admin_email = email;
}
public String getPassword() {
	return admin_password;
}
public void setPassword(String password) {
	this.admin_password = password;
}
@Override
public String toString() {
	return "Admin [email=" + admin_email + ", password=" + admin_password + "]";
}

}
