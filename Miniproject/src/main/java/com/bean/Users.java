package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Users {
	@Id
private String user_email;
private String user_password;

public Users() {
	super();
}

public Users(String email, String password) {
	super();
	this.user_email = email;
	this.user_password = password;
}

public String getEmail() {
	return user_email;
}
public void setEmail(String email) {
	this.user_email = email;
}
public String getPassword() {
	return user_password;
}
public void setPassword(String password) {
	this.user_password = password;
}
@Override
public String toString() {
	return "Users [email=" + user_email + ", password=" + user_password + "]";
}

}
