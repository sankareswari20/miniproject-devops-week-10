package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.dao.AdminDao;

@Service
public class AdminService {
	@Autowired 
	AdminDao adminDao;
	
	public String checkAdminDetails(Admin adm) {
		
		if(adminDao.existsById(adm.getEmail())) {
			Admin a=adminDao.getById(adm.getEmail());
			if(a.getPassword().equals(adm.getPassword())) {
				return "sucess";
			}else {
				return "failed to login";
			}
		}else {
			return "failed";
		}
		
	}

}
